class IC:
    def __init__(self):
        # self.clock = clock
        # self.manufacturer = manufacturer
        # self.build_date = build_date
        # self.purpose = purpose
        pass

    def clock(self, clock_frequency):
        print(f'Clock frecuency: {clock_frequency}')

    def manufacturer(self):
        print('Manufactured by David Corzo, Steven Wilson & Juan Barillas')

    def build_date(self):
        print('Built 2019-08-20')

    def purpose(self):
        print('CPU Simulator for processing and interpreting .code files')
