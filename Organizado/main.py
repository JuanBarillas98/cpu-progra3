import OpenFiles
import ALU
import Memory
import CU


def main():
    RegDict = {'A': None, 'B': None, 'C': None,
               'D': None, 'PR': None, 'IR': None}
    error_list = []

    bios_reading = OpenFiles.OpenFile(filename='bios.yml')
    bios_reading_data = bios_reading.open_bios_yaml()

    # Hereby declared the RANDOM ACCESS MEMORY FROM BIOS.YML
    random_access_memory = bios_reading_data[0]

    # Hereby declared the VISUALIZATION ATRIBUTES FORM BIOS.YML
    visualization = bios_reading_data[1]

    # Hereby declared the CLOCK FREQUENCY FROM BIOS.YML
    clock_frequency = bios_reading_data[2]

    # Now we interpret the data from the .code files
    interpreter = OpenFiles.ReadMyAlgorithm(
        random_access_memory, visualization, clock_frequency)
    valid = True
    while valid:
        filename = input(
            'Enter the code file you want to excecute (w/o the extension) → ')
        filename = 'Codes\\' + filename + '.code'
        try:
            workable_data = interpreter.open_yaml(filename=filename)
            valid = False
        except:
            error_list.append(f"error no such file or directory")
            valid = True

    # We now GET THE OPCODE and for that we make use of the function in openfiles.py
    counter = 0
    for _ in workable_data:
        workable_data[counter] = workable_data[counter].replace(' ', '')
        determinant = OpenFiles.isolate_binary_from_mnemonic(
            workable_data[counter])

        # IF IT IS IN MNEMONIC
        mnemonic_opcode = ''
        if determinant:
            # RegDict
            Instance = Memory.Registers(RegDict)
            # OPCODE AND OPERAND
            operands = ''
            for letter in workable_data[counter]:
                if letter == '0':
                    operands = operands + letter
                elif letter == '1':
                    operands = operands + letter

            # CONVERT FROM BINARY TO DECIMAL
            if operands != '':
                ram_position = ALU.string_binary_to_int(operands)

            mnemonicOperationCodeBare = workable_data[counter].replace('0', '')
            mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                '1', '')

            ####################################################### start classification of commands #######################################################
            ####################################################### start classification of commands #######################################################
            if 'OUTPUT_' in mnemonicOperationCodeBare:
                mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                    'OUTPUT_', '')
                L = mnemonicOperationCodeBare.split()
                # print(L, operands)
                what_you_want_to_console = RegDict[L[0]]
                ram_adress = ALU.string_binary_to_int(operands)
                # print(random_access_memory)
                OUTPUTIntance = CU.CU()
                OUTPUTIntance.ALU_Output(
                    what_you_want_to_console, random_access_memory, ram_adress)
                # print(random_access_memory)

            elif 'LD_' in mnemonicOperationCodeBare:
                mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                    'LD_', '')
                try:
                    RegDict = Instance.returning(
                        mnemonicOperationCodeBare, random_access_memory[ram_position])
                except:
                    error_list.append(f'error assigning ram data to register')

            elif 'AND' in mnemonicOperationCodeBare:
                mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                    'AND', '')
                operand_one = str(operands[0]) + str(operands[1])
                operand_two = str(operands[2]) + str(operands[3])

                ORInstance = ALU.ALU()
                ORresult = ORInstance.ALU_AND(operand_one, operand_two)
                # OUTPUT NEEDED TO VIEW CHANGES

            elif 'ILD_' in mnemonicOperationCodeBare:
                pass

            elif 'STR_' in mnemonicOperationCodeBare:
                pass

            elif 'OR' in mnemonicOperationCodeBare:
                mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                    'OR', '')
                operand_one = str(operands[0]) + str(operands[1])
                operand_two = str(operands[2]) + str(operands[3])

                ORInstance = ALU.ALU()
                ORresult = ORInstance.ALU_OR(operand_one, operand_two)
                # OUTPUT NEEDED TO VIEW CHANGES

            elif 'SUB' in mnemonicOperationCodeBare:
                mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                    'SUB', '')
                mnemonicOperationCodeBare = list(mnemonicOperationCodeBare)
                result = 0
                for registers in mnemonicOperationCodeBare:
                    result = int(RegDict[registers]) - result

                # IF RESULT IS 0 OR NEGATIVE
                InstanceAtributes = ALU.ALU()
                overflow = InstanceAtributes.overflowFlag(result)
                print('overflow ', overflow)
                zero = InstanceAtributes.zeroFlag(result)
                print('zero ', zero)
                negative = InstanceAtributes.negativeFlag(result)
                print('negative ', negative)

                if overflow == False:
                    RegDict[mnemonicOperationCodeBare[1]] = result
                    RegDict = Instance.dictionary_modify(RegDict)
                    if zero:
                        print(result, 'has been zero')
                    if negative:
                        print(result, 'has been negative')
                else:
                    error_list.append(
                        f'The result pending to display has not been inserted to the dictionary because of an overflow error {result} > 16')

            elif 'JMP' in mnemonicOperationCodeBare:
                pass

            elif 'JMP_N' in mnemonicOperationCodeBare:
                pass

            elif 'ADD' in mnemonicOperationCodeBare:
                mnemonicOperationCodeBare = mnemonicOperationCodeBare.replace(
                    'ADD', '')
                mnemonicOperationCodeBare = list(mnemonicOperationCodeBare)
                result = 0
                for registers in mnemonicOperationCodeBare:
                    result = int(result) + int(RegDict[str(registers)])

                # IF RESULT IS 0 OR NEGATIVE
                InstanceAtributes = ALU.ALU()
                overflow = InstanceAtributes.overflowFlag(result)
                print('overflow ', overflow)
                zero = InstanceAtributes.zeroFlag(result)
                print('zero ', zero)
                negative = InstanceAtributes.negativeFlag(result)
                print('negative ', negative)

                if overflow == False:
                    RegDict[mnemonicOperationCodeBare[1]] = result
                    RegDict = Instance.dictionary_modify(RegDict)
                    if zero:
                        print(result + 'has been zero')
                    if negative:
                        print(result + 'has been negative')
                else:
                    error_list.append(
                        f'The result pending to display has not been inserted to the dictionary because of an overflow error {result} > 16')

        # IF IT IS ALL IN BINARY
        opcode = ''
        if determinant == False:
            opcode = opcode + workable_data[counter][0] + workable_data[counter][1] + \
                workable_data[counter][2] + workable_data[counter][3]
            print(opcode)

        # CLOCK FRECUENCY FOR INSTRUCTIONS

        interpreter.delay_instructions_clock()
        print(RegDict)
        counter += 1


if __name__ == "__main__":
    main()
