import time


class OpenFile():
    def __init__(self, filename):
        self.filename = filename

    def open_bios_yaml(self):
        '''Returns a a tuple[0] that consists in the initialization of the 16 positions of RAM numbers initialized, tuple[1] is a dictionary that has the visualization information, tuple[2]  consists of returning the clock frequency; all hereby provided by the bios.yml'''
        with open(self.filename, mode='r') as f:
            List = f.readlines()

        comments = []
        code = []
        R_A_M = []
        Visualization = {'RAM': None, 'Registers': None,
                         'Clock': None, 'ALU': None}

        for element_yml in List:
            if '#' in element_yml:
                comments.append(element_yml)
            elif '#' not in element_yml:
                code.append(element_yml)

        # print(code)

        counter = 0
        for element_code in code:
            code[counter] = code[counter].replace('\n', '')
            code[counter] = code[counter].replace(' ', '')
            code[counter] = code[counter].replace('-', '')
            counter += 1
        ####################################################### prueba #######################################################
        code_counter = 0
        # print(code)
        for everything in code:

            ####################################################### RAM RELATED ACTIONS #######################################################
            if 'RAM:' in code[code_counter]:
                determinant = code[code_counter].replace('RAM:', '')

                # If the determinant is not blank then there will surely be a boolean after it, in that case we want to register it in the visualization dictionary, in the case the determinant is '' then we need to register it as a pending command that will wive us 16 positions to initialize RAM.
                if determinant == '':
                    counter_for_while = code_counter
                    # After the RAM: key word will proceed a 16 position number list, we don't want the RAM: key word, we just want the numbers, hence we increment it by one
                    counter_for_while = counter_for_while + 1
                    sixteen_assurance = 0  # To asssure we have the 16 bits
                    valid = True  # To assure the while loop is verified
                    while valid:
                        if sixteen_assurance != 16:
                            # appending numbers to R_A_M
                            R_A_M.append(code[counter_for_while])
                            sixteen_assurance = sixteen_assurance + 1  # increment by one by positions taken
                            # print(sixteen_assurance)
                        else:
                            valid = False

                        counter_for_while += 1
                ####################################################### RAM VISUALIZATION ACTIVATION #######################################################
                # We speak of booleans then it has to do with the visualizations thereby stated in bios.yml
                elif determinant == 'true':
                    Visualization['RAM'] = True

                elif determinant == 'false':
                    Visualization['RAM'] = False
            ####################################################### ALU ACTIVATION #######################################################

            ####################################################### REGISTERS ACTIVATION #######################################################
            elif 'Registers:' in code[code_counter]:
                code[code_counter] = code[code_counter].lower()
                reg_determinant = code[code_counter].replace('registers:', '')
                if 'true' in reg_determinant:
                    Visualization['Registers'] = True
                elif 'false' in reg_determinant:
                    Visualization['Registers'] = False

            elif 'Clock:' in code[code_counter]:
                # For visualization purposes the clock: in visualization must be written like this "Clock:" with the first capital letter, hereby stated in the bios.yml.
                code[code_counter] = code[code_counter].lower()
                clock_determinant = code[code_counter].replace('clock:', '')
                if 'true' in clock_determinant:
                    Visualization['Clock'] = True
                elif 'false':
                    Visualization['Clock'] = False

            elif 'ALU:' in code[code_counter]:
                code[code_counter] = code[code_counter].lower()
                alu_determinant = code[code_counter].replace('alu:', '')
                if 'true' in alu_determinant:
                    Visualization['ALU'] = True
                elif 'false' in alu_determinant:
                    Visualization['ALU'] = False

            elif 'clock:' in code[code_counter]:
                code[code_counter] = code[code_counter].lower()
                # this determines frequency for the clock and MUST BE WRITTEN IN ALL LOWERCASE LETERS IN BIOS.YML
                clock_frequency = code[code_counter].replace('clock:', '')
                try:
                    clock_frequency = float(clock_frequency)
                except:
                    clock_frequency = str(clock_frequency)

            code_counter += 1
        return R_A_M, Visualization, clock_frequency


####################################################### READ CODE #######################################################
####################################################### READ CODE #######################################################
class ReadMyAlgorithm():
    def __init__(self, R__A__M, V_I_S_U_A_L_I_Z_A_T_I_O_N, C__L__O__C__K):
        self.RAM = R__A__M
        self.VISUALIZATION = V_I_S_U_A_L_I_Z_A_T_I_O_N
        self.CLOCK = C__L__O__C__K

    def open_yaml(self, filename):
        '''This method opens the .yml and creates useful information through filtering the information isolating information not relevant to the interpreter such as comments or the famous backslash n from strings '''
        with open(filename, mode='r') as f:
            L = f.readlines()

        filtered = []
        for line in L:
            if '#' not in line:
                filtered.append(line)

        doublefiltered = []
        for hashtag in filtered:
            if hashtag != '\n':
                doublefiltered.append(hashtag)

        counter = 0
        for _ in doublefiltered:
            doublefiltered[counter] = doublefiltered[counter].replace('\n', '')
            counter += 1
        return doublefiltered

    def delay_instructions_clock(self):
        time.sleep(self.CLOCK)


def isolate_binary_from_mnemonic(workable_data_element):
    '''if mnemonic string is equal to null we are dealing with binary, otherwise we are dealing with mnemonic '''
    check = list(workable_data_element)
    mnemonic = ''
    for element in check:
        element = str(element)
        if ord(element) in range(65, 91):
            mnemonic = mnemonic + element
    if mnemonic == '':
        mnemonic = False
    else:
        mnemonic = True
    return mnemonic
