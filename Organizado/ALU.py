from IC import *
from Memory import *


def string_binary_to_int(binario):

    binario = list(binario)

    L = []

    contador_filtro = 0
    for _ in binario:
        if binario[contador_filtro] == '0':
            L.append(int(binario[contador_filtro]))
        elif binario[contador_filtro] == '1':
            L.append(int(binario[contador_filtro]))
        contador_filtro += 1

    cantidad = len(L) - 1
    contador = 0
    suma = 0

    for every in L:
        suma = suma + (int(binario[contador])*2**cantidad)
        cantidad = cantidad - 1
        contador += 1

    return suma


class ALU(IC):
    ''' Arithmetic Logic Unit, is a digital electronic circuit responsible for
     doing all the arithmetic operations '''

    # Initializer/Instance Attribute
    def __init__(self):
        pass
        # self.dataInput1 = dataInput1
        # self.dataInput2 = dataInput2

        # self.Zero = Zero
        # self.Overflow = Overflow
        # self.Negative = Negative

        # Instance methods

        # -------------------- LOGIC ---------------------------------------------------------------
        # Remember to initialize attributes of ALU p1.ALU(False, False, False)

        # Zero Flag

    def zeroFlag(self, check):
        if (check == 0) or (check == "0"):
            zero_flag = True
            print("ZERO FLAG!")
        else:
            zero_flag = False

        return zero_flag

    # initializeALU_Attributes = ALU(False, False, False)
    # print(initializeALU_Attributes.zeroFlag(checkArumentHere))

    # Overflow Flag
    def overflowFlag(self, check):
        """Evaluates overflow more than 4-bits (15)"""
        if (check > 16):
            # print("OVERFLOW FLAG!")
            over_flow = True
        else:
            over_flow = False
        return over_flow

    # p1 = ALU(False, False, False)
    # print(p1.overflowFlag(aa))

    # Negative Flag

    def negativeFlag(self, check):
        """Evaluates if number is negative"""
        negative = None

        if check < 0:
            negative = True

        elif check < 0:
            # print("NEGATIVE FLAG!")
            negative == False

        return negative

    #######################################################  #######################################################
    #######################################################  #######################################################

    # 0011 (2 bit register ID)

    def ALU_AND(self, dataInput1, dataInput2):
        """Performs AND between 2-bit registers ID"""
        # andResult =  a.dictionary[f"REG_{dataInput1}"] and a.dictionary[f"REG_{dataInput2}"]
        operated = []
        counter = 0
        for a in dataInput1:

            if int(dataInput1[counter]) == 0:
                # if dataInput2[counter] == 0:
                #     result = 0
                #     operated.append(result)
                # elif dataInput2[counter] == 1:
                #     result = 0
                result = 0
                operated.append(result)
            elif int(dataInput1[counter]) == 1:
                if int(dataInput2[counter]) == 0:
                    result = 0
                    operated.append(result)
                elif int(dataInput2[counter]) == 1:
                    result = 1
                    operated.append(result)
            counter += 1
        return operated

    # 0111 (2 bit register ID)
    def ALU_OR(self, dataInput1, dataInput2):
        """Performs OR between 2-bit registers ID"""
        operated = []
        counter = 0
        for a in dataInput1:
            if int(dataInput1[counter]) == 0:
                if int(dataInput2[counter]) == 0:
                    result = 0
                    operated.append(result)
                elif int(dataInput2[counter]) == 1:
                    result = 1
                    operated.append(result)

            elif int(dataInput1[counter]) == 1:
                if int(dataInput2[counter]) == 0:
                    result = 1
                    operated.append(result)
                elif int(dataInput2[counter]) == 1:
                    result = 1
                    operated.append(result)
            counter += 1
        return operated

    # 1001 (2 bit registers ID)
    def ALU_ADD(self, dataInput1, dataInput2):
        """Add two registers, store result into second register, RECIEVES A BINARY STRING according to the instructions the operands will always be recieved in binary"""
        try:
            dataInput1 = string_binary_to_int(dataInput1)
            dataInput2 = string_binary_to_int(dataInput2)
            return dataInput1 + dataInput2
        except:
            print('error, binary code required for addition')

    # 1010 (2 bit register ID)
    def ALU_SUB(self, dataInput1, dataInput2):
        """Subtract two registers, store result into second register"""
        try:
            dataInput1 = string_binary_to_int(dataInput1)
            dataInput2 = string_binary_to_int(dataInput2)
            # if output is negative we must check in the alu flag, if it is zero we must check
            return dataInput1 - dataInput2
        except:
            print('error, binary code required for subtraction')

####################################################### END OF ALU #######################################################
####################################################### END OF ALU #######################################################
