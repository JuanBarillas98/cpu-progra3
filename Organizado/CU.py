import IC
import random


class CU(IC.IC):
    def __init__(self):
        pass

# CREATE docker file
# Python graphical interface
# Terminar funciones CU

    # 0000 (4-bit RAM address)
    def ALU_Output(self, what_you_want_to_console, RAM, ram_adress):
        """Wires OUTPUT register directly to RAM address location (writes to console)"""
        RAM[ram_adress] = what_you_want_to_console
        print(RAM)
        return RAM

    # 001 (4-bit RAM address)
    def ALU_LDA(self):
        """Reads RAM location into register A"""
        pass

    # 0010 (4-bit RAM address)
    def ALU_LDB(self):
        """Reads RAM location into register B"""
        pass

     # 0100 (4-bit constant)
    def ALU_ILDA(self):
        """Immediate Read constant into register A"""
        pass

    # 0101 (4-bit RAM address)
    def ALU_STRA(self):
        """Write from Register A into RAM location"""
        pass

    # 0110 (4-bit RAM address)
    def ALU_STRB(self):
        """Write from Register B into RAM location"""
        pass

    # # 1011 (4-bit code address)
    def ALU_JMP(self):
        """Update Inst. Addr. Reg to new address"""
        pass

    # 1100 (4-bit memory address)
    def ALU_JMP_N(self):
        """IF ALU result was negative , update Inst. Addr. Reg to new address"""
        pass

    # 1101 (FREE)
    def FREE(self):
        """FREE"""
        pass

    # 1110 (FREE)
    def FREE(self):
        """FREE"""
        pass

    # 1111 (NA)
    def ALU_HALT(self):
        """Program done. Halt computer"""
        pass
