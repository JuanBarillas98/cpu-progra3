a = 0b11111111
aa = "111101111"

class ALU:
    ''' Arithmetic Logic Unit, is a digital electronic circuit responsible for
     doing all the arithmetic operations such as: '''

    # Initializer/Instance Attribute
    def __init__(self, Zero, Overflow, Negative):
        # self.reg = Registers()
        # self.ram = RAM()
        # self.control = CU()
        # self.responsereg = ""

        self.Zero = False
        self.Overflow = False
        self.Negative = False

    # Instance methods
    def splitOpcode(self, inputString):
        """Splits the incoming string and separates the opcode"""
        opcode = inputString[0:4]
        return opcode


# -------------------- LOGIC ---------------------------------------------------------------

    def checkZERO(self, check):
        if check == bin(0):
            self.Zero = True
        return self.Zero


    def overflowFlag(self, check):
        """Checks if it is an int or str, evaluates overflow more than 8-bits (254)"""
        if type(check) == int:
            if check > int(bin(254),2):
                self.Overflow = True

        if type(check) == str:
            if len(check) > 8:
                self.Overflow = True
                print("Happened2")

        return self.Overflow

        
p1 = ALU(False, False, False)

print(p1.overflowFlag(aa))



