# from Memory import *

# inputString = "00111010"
# operand1 = "0010"
# operand2 = "1010"


# # SELF?
# def splitOpcode(inputString):
#     """Splits the incoming string and separates the opcode"""
#     opcode = inputString[0:4]
#     return opcode


# def strToBinInt(inputString):
#     """Returns the inputstring in binary format. bin() -> str
#     THIS FUNCTION DOES NOT PRODUCE A TYPE INT!                 """
#     int_inputString = int(inputString, 2)
#     bin_inputString = bin(int_inputString)
#     return bin_inputString

####################################################### CLASS IC #######################################################
####################################################### CLASS IC #######################################################
####################################################### CLASS IC #######################################################
####################################################### CLASS IC #######################################################


# class IC:
#     def __init__(self, clock, manufacturer, build_date, purpose):
#         self.clock = clock
#         self.manufacturer = manufacturer
#         self.build_date = build_date
#         self.purpose = purpose

####################################################### CLASS CU #######################################################
####################################################### CLASS CU #######################################################
####################################################### CLASS CU #######################################################
####################################################### CLASS CU #######################################################


# class CU(IC):
#     def __init__(self):
#         self.reg = Registers()
#         self.ram = RAM()

####################################################### CLASS ALU #######################################################
####################################################### CLASS ALU #######################################################
####################################################### CLASS ALU #######################################################
####################################################### CLASS ALU #######################################################


# class ALU(IC):
#     ''' Arithmetic Logic Unit, is a digital electronic circuit responsible for
#      doing all the arithmetic operations '''

#     # Initializer/Instance Attribute
#     def __init__(self, Zero, Overflow, Negative):
#         self.reg = Registers()
#         self.ram = RAM()
#         self.control = CU()
#         self.responsereg = ""

#         self.Zero = False
#         self.Overflow = False
#         self.Negative = False

#     # Instance methods


# # -------------------- LOGIC ---------------------------------------------------------------
# # Remember to initialize attributes of ALU p1.ALU(False, False, False)

# # Zero Flag


#     def zeroFlag(self, check):
#         if check == 0:
#             self.Zero = True

#         if self.Zero == True:
#             print("ZERO FLAG!")

#         return self.Zero

# # initializeALU_Attributes = ALU(False, False, False)
# # print(initializeALU_Attributes.zeroFlag(checkArumentHere))

# # Overflow Flag
#     def overflowFlag(self, check):
#         """Checks if it is an int or str, evaluates overflow more than 8-bits (254)"""
#         if type(check) == int:
#             if check > int(bin(254), 2):
#                 self.Overflow = True

#         if type(check) == str:
#             if len(check) > 8:
#                 self.Overflow = True

#         if self.Overflow == True:
#             print("OVERFLOW FLAG!")

#         return self.Overflow

# # p1 = ALU(False, False, False)
# # print(p1.overflowFlag(aa))

# # Negative Flag

#     def negativeFlag(self, check):
#         """needed in int format, evaluates if number is negative"""

#         if check < 0b0 or 0:
#             self.Negative = True

#         if self.Negative == True:
#             print("NEGATIVE FLAG!")

#         return self.Negative

# ----------------- ARITHMRTIC ------------------------------------------------------------

# # 0000 (4-bit RAM address)
# def ALU_Output(self):
#     """Wires OUTPUT register directly to RAM address location (writes to console)"""
#     pass

# # 001 (4-bit RAM address)
# def ALU_LDA(self):
#     """Reads RAM location into register A"""
#     pass

# # 0010 (4-bit RAM address)
# def ALU_LDB(self):
#     """Reads RAM location into register B"""
#     pass

# # 0011 (2 bit register ID)
# def ALU_AND(self, twoBitReg1, twoBitReg2):
#     """Performs AND between 2-bit registers ID"""
#     andResult = (int(twoBitReg1, 2) & int(twoBitReg2, 2))
#     return andResult

# # 0100 (4-bit constant)
# def ALU_ILDA(self):
#     """Immediate Read constant into register A"""
#     pass

# # 0101 (4-bit RAM address)
# def ALU_STRA(self):
#     """Write from Register A into RAM location"""
#     pass

# # 0110 (4-bit RAM address)
# def ALU_STRB(self):
#     """Write from Register B into RAM location"""
#     pass

# # 0111 (2 bit register ID)
# def ALU_OR(self, twoBitReg1, twoBitReg2):
#     """Performs OR between 2-bit registers ID"""
#     orResult = (int(twoBitReg1, 2) | int(twoBitReg2, 2))
#     return orResult

# 1000 (4-bit RAM address)
def ALU_ILDB(self):
        """Immediate Read constant into register B"""
        pass

    # # 1001 (2 bit registers ID)
    # def ALU_ADD(self, operand1, operand2):
    #     """Add two registers, store result into second register"""
    #     addResult = bin(int(operand1, 2) + int(operand2, 2))
    #     return addResult

    # 1010 (2 bit register ID)
    # def ALU_SUB(self, operand1, operand2):
    #     """Subtract two registers, store result into second register"""
    #     subResult = bin(int(operand1, 2) - int(operand2, 2))
    #     return subResult

    # # # 1011 (4-bit code address)
    # def ALU_JMP(self):
    #     """Update Inst. Addr. Reg to new address"""
    #     pass

    # # 1100 (4-bit memory address)
    # def ALU_JMP_N(self):
    #     """IF ALU result was negative , update Inst. Addr. Reg to new address"""
    #     pass

    # # 1101 (FREE)
    # def FREE(self):
    #     """FREE"""
    #     pass

    # # 1110 (FREE)
    # def FREE(self):
    #     """FREE"""
    #     pass

    # # 1111 (NA)
    # def ALU_HALT(self):
    #     """Program done. Halt computer"""
    #     pass
# -------------------------------------------------------------------------------------------

####################################################### CLASS OPEN FILES #######################################################
####################################################### CLASS OPEN FILES #######################################################
####################################################### CLASS OPEN FILES #######################################################
####################################################### CLASS OPEN FILES #######################################################


# class OpenFiles():
#     def __init__(self, filename):
#         self.filename = filename

#     def open_bios_yaml(self):
#         '''Returns a a tuple[0] that consists in the initialization of the 16 positions of RAM numbers initialized, tuple[1] is a dictionary that has the visualization information, tuple[2]  consists of returning the clock frequency; all hereby provided by the bios.yml'''
#         with open(self.filename, mode='r') as f:
#             List = f.readlines()

#         comments = []
#         code = []
#         R_A_M = []
#         Visualization = {'RAM': None, 'Registers': None,
#                          'Clock': None, 'ALU': None}

#         for element_yml in List:
#             if '#' in element_yml:
#                 comments.append(element_yml)
#             elif '#' not in element_yml:
#                 code.append(element_yml)

#         # print(code)

#         counter = 0
#         for element_code in code:
#             code[counter] = code[counter].replace('\n', '')
#             code[counter] = code[counter].replace(' ', '')
#             code[counter] = code[counter].replace('-', '')
#             counter += 1
#         ####################################################### prueba #######################################################
#         code_counter = 0
#         # print(code)
#         for everything in code:

#             ####################################################### RAM RELATED ACTIONS #######################################################
#             if 'RAM:' in code[code_counter]:
#                 determinant = code[code_counter].replace('RAM:', '')

#                 # If the determinant is not blank then there will surely be a boolean after it, in that case we want to register it in the visualization dictionary, in the case the determinant is '' then we need to register it as a pending command that will wive us 16 positions to initialize RAM.
#                 if determinant == '':
#                     counter_for_while = code_counter
#                     # After the RAM: key word will proceed a 16 position number list, we don't want the RAM: key word, we just want the numbers, hence we increment it by one
#                     counter_for_while = counter_for_while + 1
#                     sixteen_assurance = 0  # To asssure we have the 16 bits
#                     valid = True  # To assure the while loop is verified
#                     while valid:
#                         if sixteen_assurance != 16:
#                             # appending numbers to R_A_M
#                             R_A_M.append(code[counter_for_while])
#                             sixteen_assurance = sixteen_assurance + 1  # increment by one by positions taken
#                             # print(sixteen_assurance)
#                         else:
#                             valid = False

#                         counter_for_while += 1
#                 ####################################################### RAM VISUALIZATION ACTIVATION #######################################################
#                 # We speak of booleans then it has to do with the visualizations thereby stated in bios.yml
#                 elif determinant == 'true':
#                     Visualization['RAM'] = True

#                 elif determinant == 'false':
#                     Visualization['RAM'] = False
#             ####################################################### ALU ACTIVATION #######################################################

#             ####################################################### REGISTERS ACTIVATION #######################################################
#             elif 'Registers:' in code[code_counter]:
#                 code[code_counter] = code[code_counter].lower()
#                 reg_determinant = code[code_counter].replace('registers:', '')
#                 if 'true' in reg_determinant:
#                     Visualization['Registers'] = True
#                 elif 'false' in reg_determinant:
#                     Visualization['Registers'] = False

#             elif 'Clock:' in code[code_counter]:
#                 # For visualization purposes the clock: in visualization must be written like this "Clock:" with the first capital letter, hereby stated in the bios.yml.
#                 code[code_counter] = code[code_counter].lower()
#                 clock_determinant = code[code_counter].replace('clock:', '')
#                 if 'true' in clock_determinant:
#                     Visualization['Clock'] = True
#                 elif 'false':
#                     Visualization['Clock'] = False

#             elif 'ALU:' in code[code_counter]:
#                 code[code_counter] = code[code_counter].lower()
#                 alu_determinant = code[code_counter].replace('alu:', '')
#                 if 'true' in alu_determinant:
#                     Visualization['ALU'] = True
#                 elif 'false' in alu_determinant:
#                     Visualization['ALU'] = False

#             elif 'clock:' in code[code_counter]:
#                 code[code_counter] = code[code_counter].lower()
#                 # this determines frequency for the clock and MUST BE WRITTEN IN ALL LOWERCASE LETERS IN BIOS.YML
#                 clock_frequency = code[code_counter].replace('clock:', '')
#                 try:
#                     clock_frequency = float(clock_frequency)
#                 except:
#                     clock_frequency = str(clock_frequency)

#             code_counter += 1
#         return R_A_M, Visualization, clock_frequency


# ####################################################### OPCODE #######################################################
# # Zero = ALU.Zero()
# yaml = OpenFiles(filename='bios.yml')
# Ram_Visualization_clock = yaml.open_bios_yaml()

# R__A__M = Ram_Visualization_clock[0]
# V_I_S_U_A_L_I_Z_A_T_I_O_N = Ram_Visualization_clock[1]
# C__L__O__C__K = Ram_Visualization_clock[2]

####################################################### READ THE ALGORITHM IN ARBITRARY YML FILES #######################################################
####################################################### READ THE ALGORITHM IN ARBITRARY YML FILES #######################################################
####################################################### READ THE ALGORITHM IN ARBITRARY YML FILES #######################################################
####################################################### READ THE ALGORITHM IN ARBITRARY YML FILES #######################################################


# class ReadYMLAlgorithm():
#     def __init__(self, R__A__M, V_I_S_U_A_L_I_Z_A_T_I_O_N, C__L__O__C__K):
#         self.RAM = R__A__M
#         self.VISUALIZATION = V_I_S_U_A_L_I_Z_A_T_I_O_N
#         self.CLOCK = C__L__O__C__K

#     def open_yaml(self, filename):
#         with open(filename) as f:
#             print(f.readline())

#             # for line in f:
#             #     if '#' in line:


# prueba = ReadYMLAlgorithm(R__A__M, V_I_S_U_A_L_I_Z_A_T_I_O_N, C__L__O__C__K)
# prueba.open_yaml()
