# CPU Programación III- UFM
Este proyecto consiste en crear una simulación de un CPU, conlleva diseñar la típica arquitectura de computadora a un nivel básico utilizando programación orientada a objetos.

##¿Qué clases contiene?
* IC: Ingegrated circuit, esta es la clase padre y de esta clase se heredan muchas funciones a las clases hijas.
* Memory: Esta clase hereda de IC pero es clase padre para las que vienen a continuación.
* ALU: Esta clase se encarga de hacer todas las operaciones lógicas, tales como las operaciones AND, OR, NOT, al igual que operaciones aritméticas como suma, división, resta, multiplicación. El acrónimo ALU se refiere a Arithmetic Logic Unit.
* CU: Control Unit, esta clase hereda a la IC, es la clase encargada de todo el control.
* Registers: La clase que se encarga de almacenar memoria.
* RAM: Random Access Memory, esta es la memoria de acceso aleatorio que va a ser la memoria dinámica del proyecto, desde esta se va a llevar a cabo las operaciones del ALU por ejemplo.

## Asuntos relevantes, tecnicalities about this project:
* OPCODE: This will determine the operation code
* bios.yml: This will initialize everything
* Código Binario: Es el lenguaje de las computadoras.
* 4-bit-CPU: Un CPU que procesa 4 bits como operandos después del opcode.
* Clock frequency in Hertz: la velocidad de operación en la que se ejecuta cada instrucción.


### Fases del proyecto
#### Fase No. 1:
Se hacen las clases y se definen que se hace en cada una.

#### Fase No. 2:
Se hace el ALU, la interpretación de mnemonico y binario de los comandos en OPCODE y en binario.

#### Fase No. 3:
Se crearon algoritmos en nuestros .code para el uso de nuestro programa.

####Integrantes del Grupo:
* David Corzo - actualmente cursando Programación III en la Universidad Francisco Marroquín, Guatemala, estudiando Licenciatura en Ingenería en Computer Science. 
* Steven Wilson - actualmente cursando Programación III en la Universidad Francisco Marroquín, Guatemala, estudiando Licenciatura en Ingenería en Computer Science.
* Juan Barillas - actualmente cursando Programación III en la Universidad Francisco Marroquín, Guatemala, estudiando Licenciatura en Ingenería en Computer Science. 
 
